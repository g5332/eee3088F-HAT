# Contributing Guide
Thank you for taking time to contribute to our project!:blush: To get an overview of the project, read the [README](https://gitlab.com/-/ide/project/g5332/eee3088F-HAT/tree/tris/-/README.md/). All contributors can be see [here](https://gitlab.com/g5332/eee3088F-HAT/-/graphs/main).

## Issues and Changes
### Create a new issue
If you spot a problem withor want to make changes to any files in the repository, check if an [issue already exists](https://gitlab.com/g5332/eee3088F-HAT/-/issues). If a related issue does not exist, you can open a [new issue](https://gitlab.com/g5332/eee3088F-HAT/-/issues/new).

Make sure to use the predefined issue labels to add your new issue to an existing group.

### Solving an issue
Look through our [existing issues](https://gitlab.com/g5332/eee3088F-HAT/-/issues) to find one that you would like to work on. You can use our predefined filters to make this process faster. As a general rule, we don’t assign issues to anyone. 

If you solve an issue:
- open the relevant issue in the [issues section](https://gitlab.com/g5332/eee3088F-HAT/-/issues)
- click "create merge request": this will create a new branch linked to the issue
- got to [branches](https://gitlab.com/g5332/eee3088F-HAT/-/branches)
- open the branch that that is linked to your issue
- update the branch with your changes
- create a Merge Request with
    - a descriptive title
    - a thorough description of your changes
    - a relevant label (from the predefined list)
    - all other necessary information
- we will then review your changes. 

Once the merge has been approved by us, you will be an official contributor, CONGRATULATIONS! :dancer:

## Tools For Contributing
### Schematic and PCB
To contribute to schematic and PCB design, use KiCAD and install the JLCPCB extension.
##### Guidelines and Rules to follow:
- update the revision number and add your name as a contributor
- use descriptive labels and text to explain anything that is not obvious
- when adding new components to schematic:
    - update interfacing documentation
    - add JLCPCB part numbers
    - add the component's datasheet
    - add the correct footprint
    - run ERC
    - update the BOM and include the new BOM in your merge request (at [this location](https://gitlab.com/g5332/eee3088F-HAT/-/tree/main/Docs))
- when changing PCB
    - only place components on front of the board
    - make sure layout is neat
    - add labels:
        - to any connection points on the STM32 pins 
        - to test points
        - to anything else that is not obvious
        - that are **neat and readable**
    - follow trackwidth quidelines as set up in the project
    - only change the filled pores as a last resort
    - run DRC check
    - Regenerate the gerber files include them in your merge request (at [this location](https://gitlab.com/g5332/eee3088F-HAT/-/tree/main/PCB/Gerbers))
- upload the new KiCAD project, footprints and libraries to the [PCB folder](https://gitlab.com/g5332/eee3088F-HAT/-/tree/main/PCB)
### Firmware
When contributing to the firmware in this repository, make sure to use the STMCube IDE.
##### Guidelines and Rules to follow:
- Add code to specified sections i.e. place all the "#include" statements in the same section
- **Always comment** in a meaningful way
    - Explain what code you have added, why you have added it and how it works
    - Add your name and the date of the modification/addition
    - Add references and licenses, it is important to give credit where it is due
- Update the [code document](https://gitlab.com/g5332/eee3088F-HAT/-/blob/main/Docs/Code%20Reference.docx) with your changes and include this in the merge request
- Upload the new code files with all drivers, packages,etc to the [Firmware Folder](https://gitlab.com/g5332/eee3088F-HAT/-/tree/main/Firmware)











