# EEE3088F Hat Project

## Welcome
Welcome to our STM32f051 weather sensing PCB hat project. This project is proudly designed by Tristyn Ferreiro, Hamish McKenzie and Rory Schram

## Introduction
The STM32f051 is a multipurpose microcontroller that has many useful features. We set out to design what is colloquially known as a "HAT" for this microcontroller. The STM32f051 sits on top of the HAT and connects through the appropriate pins to to provide useful sensing data. 

## Required Hardware
To get started working on this project, the following hardware is required:

| Hardware     | Quantity |
| :----------- | :-----------:|
| The HAT itself      | x 1|
| STM32f051 Discovery Board   | x 1|
| Micro-USB   | x 1 |
| Mini-USB   | x 1  |
| Male-to-Female Jumpers   | x 10  |
| Male-to-Male Jumpers   | x 10    |
| 18650 Battery   | x 1    |
| Computer to Code   | x 1    |

Note: The quantity of the jumpers stated is far beyond what is actually needed, however, jumpers are a really useful tool for debugging and so having many of them is highly recommended. 

Note: All the PCB files are located under the PCB folder. The KiCAD files can be modified and then the gerbers can be re-generated or our original gerbers can be used. 

## Required Software
As well as the above mentioned hardware, the following software/tools are needed:

| Hardware     | Quantity |
| :----------- | :-----------:|
| STM32 Cube IDE (Or equivalent) | x 1|
| Serial Port Monitor  | x 1|

Note: The latest STM software needs to be loaded onto the STM32f051 for this project, this can be done with the STM32 Cube IDE software. 

## Connecting the Hardware
The pins on the HAT and the STM32f051 need to be connected in the following way:
| HAT Pin     | STM32f051 Pin |
| :-----------: | :-----------:|
| GND | GND|
| J4 (5V)  | 5V|
| SDA1  | PB7|
| SCL1  | PB6|
| MCS  (MOSFET SWITCH) | PB5 |
| SDA | PF7 |
| SCL | PF6 |
| TP12 (RX) | PA9 |
| TP13 (TX)| PA10 |
| PD (Plug Detect) | PB1 |
| AN (Analogue Sensor) | PC0 |
| J1 (3V) | 3V |

Circuit diagram photos of the required setup can be seen below:
### The Power Circuit:
![The Power Circuit](Docs/Circuit Diagram Guide/power.png)

### The Digital Sensor Circuit:
![The Power Circuit](Docs/Circuit Diagram Guide/digital.png)

### The Analogue Sensor Circuit:
![The Power Circuit](Docs/Circuit Diagram Guide/analogue.png)

### The Communications Sensor Circuit:
![The Power Circuit](Docs/Circuit Diagram Guide/coms.png)

## Run our Program
To run our program and test if all the connections are working properly, create a clone of the repository to your local machine, do this by running the following code:
```bash
cd storage_directory
```
```bash
git clone https://gitlab.com/g5332/eee3088F-HAT.git
```
To get the latest version of the main branch, run:

```bash
git pull origin main
```
Open the STM32 Cube IDE Software and open the project file in the firmware folder called "EEE3088F_HAT_C_Code". Build the code, and then debug the code with the STM32f051 plugged in to your computer. Click the run button in the STM32 Cube IDE.

The code should now be uploaded to the STM32f051. To moniter the ouput of the board connect the micro-usb from the HAT to your computer, a serial port monitor is needed. On a mac run the following command:
```bash
screen /dev/tty.usbserial-1420
```
This will open a serial monitor in the terminal which can be used to monitor the output of the HAT. Example output will look like the following:
```bash
00:25:49, 394, 20.9ºC, 53.8% RH 

Writing 0x14 to EEPROM address 0x189
Reading from EEPROM address 0x189
Received data: 20ºC
```

If Windows is being used, install the free software package called "Putty" to monitor the serial port. Instructions on how to setup Putty can be found on the following website: https://www.putty.org 


# General Git Commands

To create a clone of this repository, run the following commands:

```bash
cd storage_directory
```
```bash
git clone https://gitlab.com/g5332/eee3088F-HAT.git
```
To get the latest version of the main branch, run:

```bash
git pull origin main
```

To create a new branch, use the following bash command:

```bash
git checkout -b "newBranchName"
```
To merge branches, use the GitLab webpage.

Add all files to working tree:

```bash
git add .
```

Commit all files with a comment:

```bash
git commit -am "Message"
```
OR
```bash
git commit -a -m "Messaage"
```

To push to GitLab:

```bash
git push origin main
```
OR
```bash
git push
```
